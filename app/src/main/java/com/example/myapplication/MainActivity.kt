package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    public lateinit var btnTrue: Button
    public lateinit var btnFalse: Button
    public lateinit var btnNext: Button
    public lateinit var tvQuestion: TextView
    public var currentIndex: Int = 0
    private val questionList = listOf(
        Question(R.string.question_android, true),
        Question(R.string.question_calculator, false),
        Question(R.string.question_unity, true),
        Question(R.string.question_json, true)
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnTrue = findViewById(R.id.btnTrue)
        btnFalse = findViewById(R.id.btnFalse)
        btnNext = findViewById(R.id.btnNext)
        tvQuestion = findViewById(R.id.tvQuestion)

        tvQuestion.setText(questionList[currentIndex].textResId)

        btnTrue.setOnClickListener {
            checkAnswer(true);
        }
        btnFalse.setOnClickListener {
            checkAnswer(false);
        }
        btnNext.setOnClickListener {
            currentIndex++;
            tvQuestion.setText(questionList[currentIndex].textResId)
        }
    }

    private fun checkAnswer(userAnswer: Boolean){
        val correctAnswer = questionList[currentIndex].answer
        val messageResId = if (userAnswer == correctAnswer) {
            R.string.correct_text
        } else {
            R.string.incorrect_text
        }
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT)
            .show()
    }

}